resource "kubernetes_config_map" "docker_daemon" {
    count = var.gitlab_pull_through || var.docker_pull_through ? 1 : 0

    metadata {
        name = "docker-daemon"
        namespace = kubernetes_namespace.gitlab_runner.metadata[0].name
    }

    data = {
        daemon = <<EOT
{
  "insecure-registries": [
    %{~ if var.docker_pull_through ~}
        "${ try(kubernetes_service.docker_registry_docker[0].metadata[0].name, "") }.${kubernetes_namespace.gitlab_runner.metadata[0].name}.svc.cluster.local:5000"
        %{~ if var.gitlab_pull_through ~},%{~ endif ~}
    %{~ endif ~}
    %{~ if var.gitlab_pull_through ~}
        "${ try(kubernetes_service.docker_registry_gitlab[0].metadata[0].name, "") }.${kubernetes_namespace.gitlab_runner.metadata[0].name}.svc.cluster.local:5000"
    %{~ endif ~}
  ],
  "registry-mirrors": [
      %{~ if var.docker_pull_through ~}
          "http://${ try(kubernetes_service.docker_registry_docker[0].metadata[0].name, "") }.${kubernetes_namespace.gitlab_runner.metadata[0].name}.svc.cluster.local:5000"
          %{~ if var.gitlab_pull_through ~},%{~ endif ~}
      %{~ endif ~}
      %{~ if var.gitlab_pull_through ~}
          "http://${ try(kubernetes_service.docker_registry_gitlab[0].metadata[0].name, "") }.${kubernetes_namespace.gitlab_runner.metadata[0].name}.svc.cluster.local:5000"
      %{~ endif ~}
  ]
}
EOT
    }
}