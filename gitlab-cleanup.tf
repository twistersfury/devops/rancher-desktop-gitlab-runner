provider "kubectl" {
    config_path = var.kube_config_path
}

data "http" "gitlab_cleanup" {
    url = "https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/-/raw/main/pod-cleanup.yml"
}

data "kubectl_file_documents" "github_cleanup" {
    content = replace(data.http.gitlab_cleanup.response_body, "/image: registry.gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup:latest/", "image: twistersfury/gitlab-runner-pod-cleanup:latest\n    env:\n     - name: POD_CLEANUP_KUBERNETES_NAMESPACES\n       value: ${var.gitlab_runner_namespace}")
}

resource "kubectl_manifest" "gitlab_cleanup" {
    for_each = data.kubectl_file_documents.github_cleanup.manifests
    yaml_body = each.value
    override_namespace = var.gitlab_runner_namespace
}
