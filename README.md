# GitLab Runner for Rancher Desktop

### Introduction

This project is a simple terraform script for configuring GitLab Runner. In theory, you can run it against 
any kubernetes environment, but it is intended to be ran on Rancher Desktop for a local runner when a production runner
cannot be used.

### Warning

This terraform script is configured to use your local kubectl config that defaults to `~/.kube/config`. If your
environment is configured for more than one context, it will be ran against the current active context. This could
result in you adding the runner to the wrong context. If you have more than one context, it is advised you use a separate
config file.

If you wish to use a different config file, you can do so by specifying the variable `kube_config_path` with the 
path to the config file you'd like to use.

### Requisites

1. `terraform`
2. GNU `make`
3. `wget` OR `curl` OR Manual Install (See Note)
4. `ssh-agent`
5. Rancher Desktop (Installed & Running)

#### wget vs curl

This tool functions by automatically downloading additional make dependencies. By default, it uses wget to do this.
You may also use `curl` by running `make MAKEFILE_DOWNLOAD_COMMAND="curl -o $@"`. Alternatively, you may manually install
the terraform sub-script by downloading it and placing it into the `bin` folder. See the Makefile for the URL.

### Setup

1. Create a file called `rancher-desktop.tfvars`
2. Navigate in GitLab to your project to add the runner and open `Settings -> CI/CD -> Runners`. You may find this on the group level by going to `CI/CD -> Runners -> Register a group runner`
3. Under `Specific runners`, copy the registration token.
4. Add the line `gitlab_runner_token="[gitlab-token]"` to `rancher-desktop.tfvars` where `[gitlab-token]` is the token from the above step.
5. Add the line `gitlab_runner_suffix="[my-name]"` to `rancher-desktop.tfvars` to help identify your runner (for teams).
6. If you wish to use a docker login for the public docker hub/registry, specify `docker_hub_username` as your docker username and `docker_hub_token` as your personal access token.
7. If you wish to use a docker login for the gitlab docker hub/registry, specify `gitlab_hub_username` as your docker username and `gitlab_hub_token` as your personal access token.
8. Check the `rancher-desktop.tfvars` file for all available configuration variables and adjust as needed. IE: `gitlab_runner_tags`
9. Run the command `make`

### Workspaces

This project uses the Terraform makefile provided at [https://gitlab.com/twistersfury/utilities/-/raw/master/make/terraform.mk](https://gitlab.com/twistersfury/utilities/-/raw/master/make/terraform.mk). This script enables workspaces
by default. The default workspace of this project is `rancher-desktop`. If you want to deploy the code to multiple environments, you may do so using `make TERRAFORM_WORKSPACE="alt-env"` where `alt-env` is the name of the environment.
You should create a file called `alt-env.tfvars` and put any variable differences in this file (IE: `kube_config_path`).

### Make Targets

1. `default` - Alias for `gitlab-runner. Default command ran when no target specified.
2. `gitlab-runner` - Runs `terraform apply`
3. `gitlab-runner-destory` - Runs `terraform destroy`
4. `gitlab-runner-rebuild` - Runs `terraform destroy` followed by `terraform apply`
5. `upgrade` - Runs `terraform destroy`, `git pull --rebase` and `gitlab-runner`.