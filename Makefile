######################################################
# Core Configurations (Do Not Edit)
######################################################

BIN_PATH := bin
MAKEFILE_JUSTNAME := $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE := $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS := -f $(MAKEFILE_JUSTNAME)

######################################################
# Core Configuration
######################################################

TERRAFORM_WORKSPACE ?= rancher-desktop

######################################################
# Default Command
######################################################

.PHONY: default
default: gitlab-runner

######################################################
# Include Terraform
######################################################
ifndef MAKEFILE_TERRAFORM
include $(BIN_PATH)/terraform.mk

$(BIN_PATH)/terraform.mk:
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v1.1.3/make/terraform.mk
endif

######################################################
# Include Ansible Helper
######################################################
$(BIN_PATH)/ansible.mk: Makefile | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v2.3.1/make/ansible.mk

ifndef MAKEFILE_ANSIBLE
include $(BIN_PATH)/ansible.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1


include $(BIN_PATH)/overrides.mk

######################################################
# GitLab
######################################################

.PHONY: upgrade
upgrade:
	make gitlab-runner-destroy TERRAFORM_AUTO=true TERRAFORM_WORKSPACE=$(TERRAFORM_WORKSPACE)
	git restore .
	git pull --rebase
	touch $(BIN_PATH)/overrides.mk
	mv $(BIN_PATH)/overrides.mk .
	rm -rf $(BIN_PATH)
	mkdir $(BIN_PATH)
	mv overrides.mk $(BIN_PATH)/overrides.mk
	make gitlab-runner TERRAFORM_AUTO=true TERRAFORM_WORKSPACE=$(TERRAFORM_WORKSPACE)

.PHONY: gitlab-runner
gitlab-runner:
	make terraform-apply TERRAFORM_DIRECTORY=. TERRAFORM_AUTO=$(TERRAFORM_AUTO) TERRAFORM_WORKSPACE=$(TERRAFORM_WORKSPACE)

.PHONY: gitlab-runner-destroy
gitlab-runner-destroy:
	make terraform-destroy TERRAFORM_DIRECTORY=. TERRAFORM_AUTO=$(TERRAFORM_AUTO) TERRAFORM_WORKSPACE=$(TERRAFORM_WORKSPACE)
	rm -rf rancher/terraform/.terraform

.PHONY: gitlab-runner-rebuild
gitlab-runner-rebuild: gitlab-runner-destroy gitlab-runner


#######################################################
# K3S Install
#######################################################

.PHONY: k3s-install
k3s-install: ANSIBLE_DIRECTORY=ansible
k3s-install: ANSIBLE_INVENTORY=inventories/
k3s-install: ANSIBLE_PLAYBOOK=install.yaml
k3s-install: ANSIBLE_VAULT_IDS=
k3s-install: ansible