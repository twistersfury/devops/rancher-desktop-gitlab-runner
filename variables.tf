variable "kube_config_path" {
    type = string
    description = "Path To Kubectl Config File"
    default = "~/.kube/config"
}

variable "gitlab_runner_namespace" {
    type = string
    description = "Namespace Name For GitLab Runner"
    default = "gitlab-runner"
}

# helm repo add gitlab https://charts.gitlab.io
# helm repo update
# helm search repo -l gitlab/gitlab-runner | cut -d$'\t' -f 2 | grep -v CHART | sort --version-sort --reverse | head -n 1
variable "gitlab_runner_version" {
    type = string
    description = "Version of Runner To Install"
    default = "0.73.3"
}

variable "gitlab_runner_helper_image" {
    type = string
    description = "Image Name to use For Helper Image. Set to registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:arm64-latest for ARM Version."
    default = "registry.gitlab.com/gitlab-org/gitlab-runner/gitlab-runner-helper:x86_64-latest"
}

variable "gitlab_runner_ttlHours" {
    type = number
    description = "Default TTL For Auto Cleaning. Set Hire Than Highest Project and/or .gitlab-ci.yml TTL"
    default = 6
}

variable "gitlab_runner_cpu" {
    type = number
    description = "Number of Micro Processors"
    default = 300
}

variable "gitlab_runner_memory" {
    type = number
    description = "Megabytes of Memory"
    default = 200
}

variable "gitlab_runner_privileged" {
    type = bool
    description = "Enable Privileged Mode (IE DinD)"
    default = true
}

variable "gitlab_runner_untagged" {
    type = bool
    description = "Enabled Un-Tagged Mode"
    default = true
}

variable "gitlab_runner_tags" {
    type = string
    description = "Tags For GitLab Runner (Escape All Commas with '\\'"
    default = "docker-buildx\\,kube"
}

variable "gitlab_runner_token" {
    type = string
    description = "GitLab Runner Token"

    validation {
        condition = length(var.gitlab_runner_token) > 0
        error_message = "GitLab Runner Token Is Required"
    }
}

variable "gitlab_runner_service_cpu_limit" {
    type = number
    description = "GitLab Runner Service CPU Micro Processors Limit"
    default = 3000
}

variable "gitlab_runner_service_cpu_request" {
    type = number
    description = "GitLab Runner Service CPU Micro Processors Request"
    default = 500
}

variable "gitlab_runner_service_memory_limit" {
    type = number
    description = "GitLab Runner Service Limit Megabytes of Memory"
    default = 3000
}

variable "gitlab_runner_service_memory_request" {
    type = number
    description = "GitLab Runner Service Request Megabytes of Memory"
    default = 100
}

variable "gitlab_runner_cpu_limit" {
    type = number
    description = "GitLab Runner Job CPU Micro Processors Limit"
    default = 1000
}

variable "gitlab_runner_cpu_request" {
    type = number
    description = "GitLab Runner Job CPU Micro Processors Request"
    default = 100
}

variable "gitlab_runner_job_memory_limit" {
    type = number
    description = "GitLab Runner Job Megabytes of Memory"
    default = 3000
}

variable "gitlab_runner_job_memory_request" {
    type = number
    description = "GitLab Runner Job Megabytes of Memory"
    default = 100
}

variable "gitlab_runner_concurrent" {
    type = number
    description = "GitLab Runner Number of Concurrent Jobs"
    default = 1
}

variable "docker_hub_username" {
    type = string
    description = "Docker Hub Username"
    default = ""
}

variable "docker_hub_token" {
    type = string
    description = "Docker Hub Access Token"
    default = ""
}

variable "gitlab_runner_suffix" {
    type = string
    description = "Suffix Added To Runner (Used To Better Identify Runners When Working on a Team)"

    validation {
        condition = length(var.gitlab_runner_suffix) > 0
        error_message = "Runner Suffix Is Required"
    }
}


variable "dind_enabled" {
    type = bool
    description = "Enable Default DinD Configuration (Does Not Include Service)"
    default = true
}

variable "dind_env" {
    type = bool
    description = "Enable Default DinD Environment Variables"
    default = true
}

variable "dind_image" {
    type = string
    description = "Image To Use For Default DinD Service"
    default = "docker:dind"
}

variable "dind_service" {
    type = bool
    description = "Configure The DinD Service"
    default = true
}

variable "mysql_mount" {
    type = bool
    description = "Enable Memory Based MySQL Mount /var/lib/mysql (Disable If Using Multiple DB Services)"
    default = true
}

variable "gitlab_hub_username" {
    type = string
    description = "GitLab Hub Username"
    default = ""
}

variable "gitlab_hub_token" {
    type = string
    description = "GitLab Hub Access Token"
    default = ""
}

variable "docker_pull_through" {
    type = bool
    description = "Enable Docker Pull Through Registry"
    default = false
}

variable "gitlab_pull_through" {
    type = bool
    description = "Enable GitLab Pull Through Registry"
    default = false
}

variable "cache_directory" {
    type = string
    description = "Default Absolute Host Path For Cache Directory (IE: Composer, NodeJS)"
    default = "/cache"
}

variable "probe_timeout" {
    type = number
    default = 70
    description = "Timeout For Helm Chart Liveness Probe"
}

variable "gitlab_runner_pull_policy" {
    type = string
    default = "always"
    description = "Configure Image Pull Policy: always, if-not-present, never"
}