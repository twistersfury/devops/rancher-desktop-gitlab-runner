resource "kubernetes_namespace" "gitlab_runner" {
    metadata {
        name = var.gitlab_runner_namespace
        labels = {
            app = "gitlab-runner"
        }
    }
}

resource "kubernetes_secret" "gitlab_runner" {
    metadata {
        name = "gitlab-runner"
        labels = {
            app = "gitlab-runner"
        }
        namespace  = kubernetes_namespace.gitlab_runner.metadata[0].name
    }

    data = {
        runner-registration-token = ""
        runner-token = var.gitlab_runner_token
    }
}

# Install cert-manager helm chart
resource "helm_release" "gitlab_runner" {
    depends_on = [
        kubernetes_config_map.docker_daemon
    ]

    repository = "https://charts.gitlab.io"
    name       = "gitlab-runner-${var.gitlab_runner_suffix}"
    chart      = "gitlab-runner"
    version    = "v${var.gitlab_runner_version}"
    namespace  = kubernetes_namespace.gitlab_runner.metadata[0].name

    set {
        name = "gitlabUrl"
        value = "https://gitlab.com"
    }

    set {
        name = "resources.limits.cpu"
        value = "${var.gitlab_runner_cpu}m"
    }

    set {
        name = "resources.requests.cpu"
        value = "${var.gitlab_runner_cpu}m"
    }

    set {
        name = "rbac.create"
        value = true
    }

    set {
        name = "resources.limits.memory"
        value = "${var.gitlab_runner_memory}M"
    }

    set {
        name = "runners.privileged"
        value = var.gitlab_runner_privileged
    }

#     set {
#         name = "runners.runUntagged"
#         value = var.gitlab_runner_untagged
#     }

#     set {
#         name = "runners.tags"
#         value = var.gitlab_runner_tags
#         type = "string"
#     }

    set {
        name = "runners.ttlHours"
        value = var.gitlab_runner_ttlHours
    }

    set {
        name = "concurrent"
        value = var.gitlab_runner_concurrent
    }

    set {
        name = "dind.enabled"
        value = var.dind_enabled
    }

    set {
        name = "dind.env_vars"
        value = var.dind_env
    }

    set {
        name = "dind.service"
        value = var.dind_service
    }

    set {
        name = "dind.image"
        value = var.dind_image
    }

    set {
        name = "mysql.mount"
        value = var.mysql_mount
    }

    set {
        name = "registry.pull_through"
        value = var.docker_pull_through && var.gitlab_pull_through
    }

    set {
        name = "directories.cache"
        value = var.cache_directory
    }

    set {
        name = "probeTimeoutSeconds"
        value = var.probe_timeout
    }

    set {
        name = "livenessProbe.failureThreshold"
        value = 10
    }

    set {
        name = "checkInterval"
        value = 10
    }

    set {
        name = "helperImage"
        value = var.gitlab_runner_helper_image
    }

    # command = ["--insecure-registry=https://${kubernetes_service.docker_registry.metadata[0].name}.${kubernetes_namespace.gitlab_runner.metadata[0].name}.svc.cluster.local:5000"]

    set {
        name = "runners.config"
        value = <<EOT
[[runners]]
  environment = [
    "npm_config_cache=/cache/nodejs"\,
    "COMPOSER_CACHE_DIR=/var/composer"\,
{{- if .Values.dind.env_vars -}}
    "DOCKER_HOST=tcp://127.0.0.1:2376"\,
    "DOCKER_TLS_VERIFY=1"\,
    "DOCKER_CERT_PATH=/certs/client"\,
    "DOCKER_TLS_CERTDIR=/certs"
{{- end -}}
  ]

  [runners.kubernetes]
    namespace = "${kubernetes_namespace.gitlab_runner.metadata[0].name}"
    image = "ubuntu:16.04"
    service_cpu_limit = "${var.gitlab_runner_service_cpu_limit}m"
    service_cpu_request = "${var.gitlab_runner_service_cpu_request}m"
    cpu_limit = "${var.gitlab_runner_cpu_limit}m"
    cpu_request = "${var.gitlab_runner_cpu_request}m"
    helper_image = "${var.gitlab_runner_helper_image}"
    helper_cpu_limit = "1000m"
    helper_cpu_request = "100m"
    memory_limit = "${var.gitlab_runner_job_memory_limit}Mi"
    memory_request = "${var.gitlab_runner_job_memory_request}Mi"
    service_memory_limit = "${var.gitlab_runner_service_memory_limit}Mi"
    service_memory_request = "${var.gitlab_runner_service_memory_request}Mi"
    privileged = ${var.gitlab_runner_privileged}
    poll_timeout = 600
    pull_policy = "${var.gitlab_runner_pull_policy}"
    cache_dir = "/cache"

  [runners.kubernetes.pod_annotations]
    # https://gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/-/blob/main/docs/README.md
    "pod-cleanup.gitlab.com/ttl" = "{{ .Values.runners.ttlHours }}h20m"

{{ if .Values.dind.service }}
    [[runners.kubernetes.services]]
      name = "{{ .Values.dind.image }}"
      alias = "docker"
{{ end }}

    [[runners.kubernetes.volumes.empty_dir]]
      name = "docker-certs"
      mount_path = "/certs"
      medium = "Memory"

    [[runners.kubernetes.volumes.empty_dir]]
      name = "dind-storage"
      mount_path = "/var/lib/docker"

    [[runners.kubernetes.volumes.host_path]]
      name = "hostpath-modules"
      mount_path = "/lib/modules"
      read_only = true
      host_path = "/lib/modules"

#     [[runners.kubernetes.volumes.host_path]]
#       name = "hostpath-cgroup"
#       mount_path = "/sys/fs/cgroup"
#       host_path = "/sys/fs/cgroup"

    [[runners.kubernetes.volumes.host_path]]
      name = "gitlab-cache"
      mount_path = "/cache"
      host_path = "{{ .Values.directories.cache }}/service"

    [[runners.kubernetes.volumes.host_path]]
      name = "composer-cache"
      mount_path = "/var/composer"
      host_path = "{{ .Values.directories.cache }}/composer"

    [[runners.kubernetes.volumes.host_path]]
      name = "npm-cache"
      mount_path = "/cache/nodejs"
      host_path = "{{ .Values.directories.cache }}/nodejs"

{{ if .Values.registry.pull_through }}
    [[runners.kubernetes.volumes.config_map]]
      name = "docker-daemon"
      mount_path = "/etc/docker/daemon.json"
      sub_path = "daemon"
{{ end }}

{{ if .Values.mysql.mount }}
    [[runners.kubernetes.volumes.empty_dir]]
      name = "mysql-tmpfs"
      mount_path = "/var/lib/mysql"
      medium = "Memory"
{{ end }}

EOT
    }

    set {
        name = "runnerRegistrationToken"
        value = ""
    }

    set {
        name = "runners.secret"
        value = kubernetes_secret.gitlab_runner.metadata[0].name
    }
}