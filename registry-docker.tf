resource "kubernetes_deployment" "docker_registry_docker" {
    depends_on = [
        kubernetes_config_map.docker_registry_docker
    ]

    count = var.docker_pull_through ? 1 : 0

    metadata {
        name = "docker-registry-docker"
        namespace = kubernetes_namespace.gitlab_runner.metadata[0].name

        labels = {
            app = "docker-registry-docker"
        }
    }

    spec {
        selector {
            match_labels = {
                app = "docker-registry-docker"
            }
        }

        template {
            metadata {
                name = "docker-registry-docker"

                labels = {
                    app = "docker-registry-docker"
                }
            }

            spec {
                container {
                    name = "docker-registry-docker"
                    image = "registry:latest"

                    port {
                        container_port = 5000
                        name = "registry"
                    }

                    liveness_probe {
                        initial_delay_seconds = 60
                        period_seconds = 10
                        timeout_seconds = 3
                        http_get {
                            port = "5000"
                        }
                    }

                    resources {
                        limits = {
                            cpu    = "1000m"
                            memory = "1000Mi"
                        }

                        requests = {
                            cpu    = "50m"
                            memory = "50Mi"
                        }
                    }

                    volume_mount {
                        mount_path = "/etc/docker/registry/config.yml"
                        name       = "registry-config"
                        sub_path   = "config"
                    }

                    volume_mount {
                        mount_path = "/var/lib/registry"
                        name       = "registry-mount"
                    }
                }

                volume {
                    name = "registry-config"
                    config_map {
                        name = "docker-registry-docker"
                    }
                }

                volume {
                    name = "registry-mount"
                    host_path {
                        path = "/mnt/registry-docker"
                    }
                }
            }
        }
    }
}

resource "kubernetes_service" "docker_registry_docker" {
    count = var.docker_pull_through ? 1 : 0

    metadata {
        name = "docker-registry-docker"

        namespace = kubernetes_namespace.gitlab_runner.metadata[0].name

        labels = {
            app = "docker-registry-docker"
        }
    }

    spec {
        type = "ClusterIP"

        selector = {
            app = "docker-registry-docker"
        }

        port {
            port = 5000
            target_port = "registry"
        }
    }
}

resource "kubernetes_config_map" "docker_registry_docker" {
    count = var.docker_pull_through ? 1 : 0

    metadata {
        name = "docker-registry-docker"
        namespace = kubernetes_namespace.gitlab_runner.metadata[0].name
    }

    data = {
        config = <<EOT

version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: /var/lib/registry
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3

proxy:
    remoteurl: https://registry-1.docker.io
    username: ${var.docker_hub_username}
    password: ${var.docker_hub_token}

EOT
    }
}
