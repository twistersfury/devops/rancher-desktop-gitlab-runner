terraform {
    required_providers {
        kubectl-orig = {
            source  = "gavinbunney/kubectl"
            version = ">= 1.7.0"
        }

        kubectl = {
            source  = "alekc/kubectl"
            version = "~>2.0"
        }
    }
}