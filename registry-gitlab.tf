resource "kubernetes_deployment" "docker_registry_gitlab" {
    count = var.gitlab_pull_through ? 1 : 0

    depends_on = [
        kubernetes_config_map.docker_registry_gitlab
    ]

    metadata {
        name = "docker-registry-gitlab"
        namespace = kubernetes_namespace.gitlab_runner.metadata[0].name

        labels = {
            app = "docker-registry-gitlab"
        }
    }

    spec {
        selector {
            match_labels = {
                app = "docker-registry-gitlab"
            }
        }

        template {
            metadata {
                name = "docker-registry-gitlab"

                labels = {
                    app = "docker-registry-gitlab"
                }
            }

            spec {
                container {
                    name = "docker-registry-gitlab"
                    image = "registry:latest"

                    liveness_probe {
                        initial_delay_seconds = 60
                        period_seconds = 10
                        timeout_seconds = 3
                        http_get {
                            port = "5000"
                        }
                    }

                    resources {
                        limits = {
                            cpu    = "1000m"
                            memory = "1000Mi"
                        }

                        requests = {
                            cpu    = "50m"
                            memory = "50Mi"
                        }
                    }

                    port {
                        container_port = 5000
                        name = "registry"
                    }

                    volume_mount {
                        mount_path = "/etc/docker/registry/config.yml"
                        name       = "registry-config"
                        sub_path   = "config"
                    }

                    volume_mount {
                        mount_path = "/var/lib/registry"
                        name       = "registry-mount"
                    }

                }

                volume {
                    name = "registry-config"
                    config_map {
                        name = "docker-registry-gitlab"
                    }
                }

                volume {
                    name = "registry-mount"
                    host_path {
                        path = "/mnt/registry-gitlab"
                    }
                }
            }
        }
    }
}

resource "kubernetes_service" "docker_registry_gitlab" {
    count = var.gitlab_pull_through ? 1 : 0

    metadata {
        name = "docker-registry-gitlab"

        namespace = kubernetes_namespace.gitlab_runner.metadata[0].name

        labels = {
            app = "docker-registry-gitlab"
        }
    }

    spec {
        type = "ClusterIP"

        selector = {
            app = "docker-registry-gitlab"
        }

        port {
            port = 5000
            target_port = "registry"
        }
    }
}

resource "kubernetes_config_map" "docker_registry_gitlab" {
    count = var.gitlab_pull_through ? 1 : 0

    metadata {
        name = "docker-registry-gitlab"
        namespace = kubernetes_namespace.gitlab_runner.metadata[0].name
    }

    data = {
        config = <<EOT

version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: /var/lib/registry
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3

proxy:
    remoteurl: https://registry.gitlab.com
    username: ${var.gitlab_hub_username}
    password: ${var.gitlab_hub_token}

EOT
    }
}

