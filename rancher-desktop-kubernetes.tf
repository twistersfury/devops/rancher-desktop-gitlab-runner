# Kubernetes resources

# Kubernetes provider
provider "kubernetes" {
    config_path = var.kube_config_path
}
